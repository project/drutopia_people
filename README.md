<!-- writeme -->
Drutopia People
===============

Drutopia People provides a People content type for showing visitors information about staff, volunteers, contributors, and more.

 * https://www.drupal.org/project/drutopia_people
 * Issues: https://www.drupal.org/project/issues/drutopia_people
 * Source code: https://git.drupalcode.org/project/drutopia_people/-/tree/2.0.x
 * Keywords: person, profile, biography, staff, volunteers, content, drutopia
 * Package name: drupal/drutopia_people


### Requirements

 * drupal/config_actions ^1.1
 * drupal/drutopia_core ^2
 * drupal/drutopia_seo ^2
 * drupal/ds ^3.7
 * drupal/field_group ^3.0
 * drupal/pathauto ^1.8
 * drupal/paragraphs ^1.12


### License

GPL-2.0+

<!-- endwriteme -->
